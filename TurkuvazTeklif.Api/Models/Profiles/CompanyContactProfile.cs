﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TurkuvazTeklif.Data.Entities;
using TurkuvazTeklif.Models.CompanyContactModels;

namespace TurkuvazTeklif.Models.Profiles
{
    public class CompanyContactProfile : Profile

    {
        public CompanyContactProfile()
        {
            CreateMap<CompanyContact, CompanyContactInfo>()
                .ReverseMap();
            CreateMap<CompanyContact, CompanyContactRequestInfo>()
                .ReverseMap();
            CreateMap<CompanyContact, CompanyCreationContact>()
                .ReverseMap();
        }
    }
    
}
