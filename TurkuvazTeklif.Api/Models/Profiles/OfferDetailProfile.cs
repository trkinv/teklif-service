﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TurkuvazTeklif.Data.Entities;
using TurkuvazTeklif.Models.OfferModels;

namespace TurkuvazTeklif.Models.Profiles
{
    public class OfferDetailProfile : Profile
    {
        public OfferDetailProfile()
        {
            CreateMap<OfferDetail, OfferDetailsInfo>().
                ReverseMap();
            CreateMap<OfferDetail, OfferDetailsRequestInfo>().
                ReverseMap();
        }
    }
}
