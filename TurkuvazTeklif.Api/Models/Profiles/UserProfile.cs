﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TurkuvazTeklif.Data.Entities;
using TurkuvazTeklif.Models.UserModels;

namespace TurkuvazTeklif.Models.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserInfo>()
                .ReverseMap();
            CreateMap<User, UserRequestInfo>()
                .ReverseMap();
            CreateMap<UserUpdateInfo,User>()
                .ReverseMap();
        }
    }
}
