﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TurkuvazTeklif.Data.Entities;
using TurkuvazTeklif.Models.CompanyContactModels;
using TurkuvazTeklif.Models.CompanyModels;

namespace TurkuvazTeklif.Models.Profiles
{
    public class CompanyProfile : Profile
    {
        public CompanyProfile()
        {
            CreateMap<Company, CompanyInfo>()
                .ReverseMap();
            CreateMap<Company, CompanyItem>()
                .ReverseMap();
            CreateMap<Company, CompanyRequestInfo>()
                .ReverseMap();

        }
    }
}
