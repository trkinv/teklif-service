﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TurkuvazTeklif.Common.Auxiliary;
using TurkuvazTeklif.Models.UnifiedModels;
using TurkuvazTeklif.Services;

namespace TurkuvazTeklif.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly DashboardService DashboardService;

        public DashboardController( DashboardService DashboardService)
        {
            this.DashboardService = DashboardService;
        }

        [HttpGet]
        [Route("startup")]
        public async Task<Result<DashboardInfo>> StartUp()
        {
            return await  DashboardService.DashboardStartup();
        }
    }
}
