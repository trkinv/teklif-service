﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TurkuvazTeklif.Data.Entities;

namespace TurkuvazTeklif.Data.Configurations
{
    public class OfferHeaderConfiguration : IEntityTypeConfiguration<OfferHeader>
    {
        public void Configure(EntityTypeBuilder<OfferHeader> builder)
        {
            builder.ToTable("OfferHeader");

            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Createby)
                .WithMany(p => p.CreatedOffers)
                .HasForeignKey(p => p.CreatebyId)
                .HasConstraintName("FK_OfferHeader_User");

            builder.HasOne(p => p.Updateby)
                .WithMany(p => p.UpdatedOffers)
                .HasForeignKey(p => p.UpdatebyId)
                .HasConstraintName("FK_OfferHeader_User1");

            builder.HasOne(p => p.OfferCompany)//OfferHeaders olarak isimlendirdim Company kısmında
                .WithMany(p => p.OfferHeaders) //ve bunların config dosyaları CompanyConfiguration classında olmayacakmı
                .HasForeignKey(p => p.CompanyId)
                .HasConstraintName("FK_OfferHeader_Company");

            builder.HasOne(p => p.OfferCompanyContact)
                .WithMany(p => p.OfferHeaders)
                .HasForeignKey(p => p.CompanyContactId)
                .HasConstraintName("FK_OfferHeader_CompanyContact");

        }
    }
}
