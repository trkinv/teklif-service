﻿using System.Collections.Generic;
using TurkuvazTeklif.Data.Base;

namespace TurkuvazTeklif.Data.Entities
{
    public class User : Entity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte Role { get; set; }
        public bool IsActive { get; set; }


        // Navigation Properties
        public virtual ICollection<OfferHeader> CreatedOffers { get; set; }
        public virtual ICollection<OfferHeader> UpdatedOffers { get; set; }
    }
}
